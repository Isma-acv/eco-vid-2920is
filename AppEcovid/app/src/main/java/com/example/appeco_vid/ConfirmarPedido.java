package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.Clase_Productos;
import com.example.appeco_vid.VIEWMODELS.VM_Confirmar;
import com.example.appeco_vid.VIEWMODELS.VM_Productos;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmarPedido extends AppCompatActivity {
    public TextView ttitulo, tdeatelle, tfolio;
    public  String titulo, detalle, folio;
public String idcomercio, tokencomercio;
public EditText ecosto, etiempo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_pedido);
         ttitulo = (TextView) findViewById(R.id.textViewtitulo);
         tdeatelle = (TextView) findViewById(R.id.textView2detalle);
         etiempo =  (EditText) findViewById(R.id.editTextTextimepo);
        ecosto= (EditText) findViewById(R.id.editTextTextcosto);
         tfolio = (TextView) findViewById(R.id.textView3folio);

         titulo = getIntent().getStringExtra("titulo");
         detalle = getIntent().getStringExtra("detalle");
         folio = getIntent().getStringExtra("folio");
        ttitulo.setText(titulo);
        tdeatelle.setText(detalle);
        tfolio.setText(folio);



        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            tokencomercio = token;
        }
        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            idcomercio = id;
        }



    }//Oncreate


    @Override
    protected void onStop() {
        super.onStop();
        ttitulo.setText("");
        tdeatelle.setText("");
        tfolio.setText("");
    }

    public void confirmar(View view){

        String costo  =ecosto.getText().toString();
        String timpo  =etiempo.getText().toString();


        ServicioPeticion service = Api.getApi(ConfirmarPedido.this).create(ServicioPeticion.class);
        Call<VM_Confirmar> iniciarCall = service.get_Confirmar(folio,costo,timpo,tokencomercio,idcomercio);
        iniciarCall.enqueue(new Callback<VM_Confirmar>() {
            @Override
            public void onResponse(Call<VM_Confirmar> call, Response<VM_Confirmar> response) {

                VM_Confirmar peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(ConfirmarPedido.this,"No se obtuvo",Toast.LENGTH_SHORT).show();
                    return;
                }else {

                    Toast.makeText(ConfirmarPedido.this,"SE CONFIRMO EL PEDIDO",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<VM_Confirmar> call, Throwable t) {
                Toast.makeText(ConfirmarPedido.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });
    }
}//Clase