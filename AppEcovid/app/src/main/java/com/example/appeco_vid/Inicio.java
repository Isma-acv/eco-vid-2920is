package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Inicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }

    public void Gologeo(View view){
        Intent go = new Intent(Inicio.this,MainActivity.class);
        startActivity(go);
    }
    public void GoRegistro(View view){
        Intent go = new Intent(Inicio.this,Registro.class);
        startActivity(go);
    }
}