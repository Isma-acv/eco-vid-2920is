package com.example.appeco_vid.VIEWMODELS;

public class VM_Crearventa {
    public String titulo;
    public String contenido;
    public String tokencliente;
    public String idcliente;
    public String estado;
    public String municipio;
    public String colonia;
    public String calle;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getTokencliente() {
        return tokencliente;
    }

    public void setTokencliente(String tokencliente) {
        this.tokencliente = tokencliente;
    }

    public String getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(String idcliente) {
        this.idcliente = idcliente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstadoe() {
        return estadoe;
    }

    public void setEstadoe(String estadoe) {
        this.estadoe = estadoe;
    }

    public String cp;
    public String folio;
    public String detalle;
    public String estadoe;

    public VM_Crearventa(String titulo, String contenido, String tokencliente, String idcliente, String estado, String municipio, String colonia, String calle, String cp, String folio) {
        this.titulo = titulo;
        this.contenido = contenido;
        this.tokencliente = tokencliente;
        this.idcliente = idcliente;
        this.estado = estado;
        this.municipio = municipio;
        this.colonia = colonia;
        this.calle = calle;
        this.cp = cp;
        this.folio = folio;
    }


}
