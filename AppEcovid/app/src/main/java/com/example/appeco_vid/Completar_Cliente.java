package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_CompletarCliente;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;
import com.google.android.gms.common.util.NumberUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Completar_Cliente extends AppCompatActivity {

    public String Estado= "Mexico", Municipio = "Tecamac", sid,stoken,susuario;
    public EditText edttelefono,edtcolonia,edtcalle,edtcp,edtcalleuno,edtcalledos,edtreferencia;
    public String telefono,colonia,calle,calleuno,calledos,cp,refrencia;
    public String tipocuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completar__cliente);
        edttelefono = (EditText) findViewById(R.id.editTextTextNUmerocel);
        edtcolonia = (EditText) findViewById(R.id.editTextTextcolonia);
        edtcalle = (EditText) findViewById(R.id.editTextTextcalle);
        edtcalleuno = (EditText) findViewById(R.id.editTextTextcalleuno);
        edtcalledos = (EditText) findViewById(R.id.editTextTextcalledos);
        edtreferencia = (EditText) findViewById(R.id.editTextTextreferencia);
        edtcp = (EditText) findViewById(R.id.editTextTextcp);

        // obtengo mis elemtos almacenados
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            stoken=token;
        }
        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            sid=id;
        }
        //ver nombre de usuario
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
           susuario = username;
        }
        //ver el tipo de cuenta
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = preferenciascuenta.getString("CUENTA", "");
        if (cuenta != ""){
            tipocuenta = cuenta;
        }


    }//oncreate

    public void llenardatos(View view){

        telefono = edttelefono.getText().toString();
        colonia = edtcolonia.getText().toString();
        calle = edtcalle.getText().toString();
        calleuno = edtcalleuno.getText().toString();
        calledos = edtcalledos.getText().toString();
        refrencia = edtreferencia.getText().toString();
        cp = edtcp.getText().toString();

        if(TextUtils.isEmpty(telefono)){
            edttelefono.setError("Es necesario este campo");
            edttelefono.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(colonia)){
            edtcolonia.setError("Es necesario este campo");
            edtcolonia.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(calle)){
            edtcalle.setError("Es necesario este campo");
            edtcalle.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(cp)){
            edtcp.setError("Es necesario este campo");
            edtcp.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(calleuno)){
            edtcalleuno.setError("Es necesario este campo");
            edtcalleuno.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(calledos)){
            edtcalledos.setError("Es necesario este campo");
            edtcalledos.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(refrencia)){
            edtreferencia.setError("Es necesario este campo");
            edtreferencia.requestFocus();
            return;
        }

        // peticion para acompletar mi servicio

        ServicioPeticion service = Api.getApi(Completar_Cliente.this).create(ServicioPeticion.class);
        Call<VM_CompletarCliente> iniciarCall = service.get_CCliente(sid,susuario,stoken,telefono,Estado,Municipio,colonia,cp,calle,calleuno,calledos,refrencia);
        iniciarCall.enqueue(new Callback<VM_CompletarCliente>() {
            @Override
            public void onResponse(Call<VM_CompletarCliente> call, Response<VM_CompletarCliente> response) {
                VM_CompletarCliente peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Completar_Cliente.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estadoe == "true"){
                    Toast.makeText(Completar_Cliente.this,"DATOS ALMACENADOS",Toast.LENGTH_LONG).show();
                    if (tipocuenta.equals("cliente")){
                        Intent go = new Intent(Completar_Cliente.this,Panel.class);
                        startActivity(go);
                    }
                    else if (tipocuenta.equals("comerciante")){
                        Intent gio = new Intent(Completar_Cliente.this,PanelComerciante.class);
                        startActivity(gio);
                    }


                }
            }

            @Override
            public void onFailure(Call<VM_CompletarCliente> call, Throwable t) {
                Toast.makeText(Completar_Cliente.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });
    }

}//clase