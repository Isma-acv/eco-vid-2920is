package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_CompletarCliente;
import com.example.appeco_vid.VIEWMODELS.VM_CompletarTienda;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Completarcomerciante extends AppCompatActivity {
    String seleccion;
    public String idtienda,tokentienda,usuariotineda;
    private Spinner spinnercuenta;
    public String Municipio = "Tecamac", Estado="Mexico";
    public EditText enombret, etele,ecoreo,ecolo,ecp;
    public String tipocuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completarcomerciante);

        //obtengo id,token,usuario

        //ver token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            tokentienda = token;
        }
        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            idtienda = id;
        }
        //ver nombre de usuario
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
            usuariotineda = username;
        }
        //ver el tipo de cuenta
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = preferenciascuenta.getString("CUENTA", "");
        if (cuenta != ""){
            tipocuenta = cuenta;
        }


        spinnercuenta = (Spinner) findViewById(R.id.spinnergiro);
        String [] opciones = {"Panaderia","Dulceria","Tortilleria","Abarrotes","Carnicerias","Construccion","Cremerias","Farmacias","Ferreterias","Papelerias","Pinturas","Veterinarias"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, opciones);
        spinnercuenta.setAdapter(adapter);

        enombret = (EditText) findViewById(R.id.edtnombretienda);
        etele = (EditText) findViewById(R.id.edtnumerocel);
        ecoreo = (EditText) findViewById(R.id.edtcorreotienda);
        ecolo = (EditText) findViewById(R.id.edtcoloniatienda);
        ecp = (EditText) findViewById(R.id.edtcodigipostal);



    }//oncreate

    public void guardartienda(View view){
        seleccion = spinnercuenta.getSelectedItem().toString();

        String ntienda = enombret.getText().toString();
        String telefono = etele.getText().toString();
        String correo = ecoreo.getText().toString();
        String colonia = ecolo.getText().toString();
        String cp = ecp.getText().toString();


        if(TextUtils.isEmpty(ntienda)){
            enombret.setError("Es necesario este campo");
            enombret.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(telefono)){
            etele.setError("Es necesario este campo");
            etele.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(correo)){
            ecoreo.setError("Es necesario este campo");
            ecoreo.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(colonia)){
            ecolo.setError("Es necesario este campo");
            ecolo.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(cp)){
            ecp.setError("Es necesario este campo");
            ecp.requestFocus();
            return;
        }


        //peticon de registro
        ServicioPeticion service = Api.getApi(Completarcomerciante.this).create(ServicioPeticion.class);
        Call<VM_CompletarTienda> iniciarCall = service.get_tienda(idtienda,usuariotineda,ntienda,seleccion,telefono,correo,Estado,Municipio,colonia,cp,tokentienda);
        iniciarCall.enqueue(new Callback<VM_CompletarTienda>() {
            @Override
            public void onResponse(Call<VM_CompletarTienda> call, Response<VM_CompletarTienda> response) {
                VM_CompletarTienda peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Completarcomerciante.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estadoe == "true"){

                    Toast.makeText(Completarcomerciante.this,"DATOS ALMACENADOS",Toast.LENGTH_LONG).show();

                    if (tipocuenta.equals("cliente")){
                        Intent go = new Intent(Completarcomerciante.this,Panel.class);
                        startActivity(go);
                    }
                    else if (tipocuenta.equals("comerciante")){
                        Intent gio = new Intent(Completarcomerciante.this,PanelComerciante.class);
                        startActivity(gio);
                    }
                }else {
                    Toast.makeText(Completarcomerciante.this,"error"+peticion.detalle,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VM_CompletarTienda> call, Throwable t) {
                Toast.makeText(Completarcomerciante.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });

    }

}//clase