package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PanelComerciante extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel_comerciante);
        TextView idusuario = (TextView) findViewById(R.id.textView15id);
        TextView nombreusuario = (TextView) findViewById(R.id.textView9usuario);
        TextView tvcuenta = (TextView) findViewById(R.id.textView16cuenta);
        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            idusuario.setText("ID: "+id);
        }
        //ver nombre de usuario
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
            nombreusuario.setText("Usuario: "+username);
        }
        //ver el tipo de cuenta
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = preferenciascuenta.getString("CUENTA", "");
        if (cuenta != ""){
            tvcuenta.setText("Tipo de cuenta: "+cuenta);
        }


    }//oncreate


    //llevarme agregar producto
    public void goproducto(View view){
        Intent go = new Intent(PanelComerciante.this,AgregarProductos.class);
        startActivity(go);
    }
}//clase