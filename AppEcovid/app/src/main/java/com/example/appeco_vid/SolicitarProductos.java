package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_Crearventa;
import com.example.appeco_vid.VIEWMODELS.VM_Datoscliente;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SolicitarProductos extends AppCompatActivity {

    public String tokenAlmacenado, tokentienda, sestado,smunicipio,scolonia,scalle,scp,SID ;
    EditText ettitulo, etdescripcion;
    TextView tvfolio;
    public String titulo, folio, detalle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_productos);
        final Random myRandom = new Random();
        ettitulo = (EditText) findViewById(R.id.editTextTextTitulo);
        etdescripcion = (EditText) findViewById(R.id.editTextTextDescricion);
        tvfolio = (TextView) findViewById(R.id.textViewFolio);
        String num1=String.valueOf(myRandom.nextInt(100));
        String num2=String.valueOf(myRandom.nextInt(1000));
        tvfolio.setText(num1+num2);


        //--------------OBTENGO EL TONKEN DE LA TIENDA QUE ME PASO EL ACTIVITY ANTERIOR
        tokentienda  = getIntent().getStringExtra("tokencomercio");

        // OBTENER TOKEN DEL DISPOSITIVO
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            tokenAlmacenado = token;
        }

        //OBTENER DATOS ALMACENADOS DE LA DIRECCION

        //obtengo los datos de la calle
        SharedPreferences preferenciaestado = getSharedPreferences("credencialesestado", Context.MODE_PRIVATE);
        String estado = preferenciaestado.getString("ESTADO", "");
        if (estado != ""){
            sestado=estado;
        }
        SharedPreferences preferenciamunicipio = getSharedPreferences("credencialesmunicipio", Context.MODE_PRIVATE);
        String municipio = preferenciamunicipio.getString("MUNICIPIO", "");
        if (municipio != ""){
            smunicipio=municipio;
        }
        SharedPreferences preferenciacolonia = getSharedPreferences("credencialescolonia", Context.MODE_PRIVATE);
        String colonia = preferenciacolonia.getString("COLONIA", "");
        if (colonia != ""){
            scolonia=colonia;
        }
        SharedPreferences preferenciacalle = getSharedPreferences("credencialescalle", Context.MODE_PRIVATE);
        String calle = preferenciacalle.getString("CALLE", "");
        if (calle != ""){
            scalle=calle;
        }
        SharedPreferences preferenciacp = getSharedPreferences("credencialescp", Context.MODE_PRIVATE);
        String cp = preferenciacp.getString("CP", "");
        if (cp != ""){
            scp=cp;
        }
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            SID= id;
        }



        // buton para crear la notificcacion
        final Button solicitarProductos = (Button) findViewById(R.id.buttonSolicitarProductos);
        solicitarProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrearNotiProductos();
            }
        });//button onclik solictar produtos


    }//oncreate

    //-- Crear notificacion
    public void CrearNotiProductos(){

        //obtengo los valores
        titulo = ettitulo.getText().toString();
        detalle = etdescripcion.getText().toString();
        folio = tvfolio.getText().toString();


        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            String token = tokenAlmacenado;
            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo",titulo);
            notificacion.put("detalle",detalle);
            notificacion.put("folio",folio);
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json, null,null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String,String> header= new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAJRPnpTw:APA91bEY0VUrx8g3nfdX067hwuCoLSRpPV-Xycsj7HNwvFZ-j6HfSZu5k7fGVl4bBEDovfdkpQBAy73UGrXjGZ0XjG2Q0cLCz7w3SRNIYwUZB9E11ftt4t2qxIUdAgvhOYKRxpuLUgLP");
                    return header;
                }
            };
            myrequest.add(request);


        }catch (JSONException e){
            e.printStackTrace();
        }


        //MANDO A LLAMAR MI METODO DE CREAR VENTA EN LA BD REMOTA
        SolicitarProductos();
    }//CrearNotificacion


    //-- creo registro de la venta en la BD
    public void SolicitarProductos(){

        if(TextUtils.isEmpty(titulo)){
            ettitulo.setError("Porfavor Ingrese un titulo");
            ettitulo.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(detalle)){
            etdescripcion.setError("Detalla del pedido");
            etdescripcion.requestFocus();
            return;
        }


        ServicioPeticion service = Api.getApi(SolicitarProductos.this).create(ServicioPeticion.class);
        Call<VM_Crearventa> iniciarCall = service.get_Venta(titulo,detalle,tokenAlmacenado,SID,sestado,smunicipio,scolonia,scalle,scp,folio);
        iniciarCall.enqueue(new Callback<VM_Crearventa>() {
            @Override
            public void onResponse(Call<VM_Crearventa> call, Response<VM_Crearventa> response) {
                VM_Crearventa peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(SolicitarProductos.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estadoe == "true"){

                        Toast.makeText(SolicitarProductos.this,""+peticion.detalle,Toast.LENGTH_LONG).show();
                    Intent go = new Intent(SolicitarProductos.this,Panel.class);
                    startActivity(go);

                    }else {
                        Toast.makeText(SolicitarProductos.this,""+peticion.detalle,Toast.LENGTH_LONG).show();

                    }
            }

            @Override
            public void onFailure(Call<VM_Crearventa> call, Throwable t) {
                Toast.makeText(SolicitarProductos.this,"Ups! Algo salio mal"+t,Toast.LENGTH_LONG).show();
            }
        });
    }
}//clase