package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.ComerciosVo;
import com.example.appeco_vid.VIEWMODELS.VM_ActivarCuenta;
import com.example.appeco_vid.VIEWMODELS.VM_Datoscliente;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Panel extends AppCompatActivity {
    //-- elemntos de mi recyclerview
    ArrayList<ComerciosVo> listacomercios;
    RecyclerView recyclerViewComercios;
    public String idcliente;
public String ESTADO,MUNICIPIO,COLONIA,CALLE,CP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);
        //obtengo los datos de inicio de secion
        //elementos de mi activity
        TextView idusuario = (TextView) findViewById(R.id.textViewIdUsuario);
        TextView nombreusuario = (TextView) findViewById(R.id.textViewUsername);
        TextView usuariotoken = (TextView) findViewById(R.id.textViewtoken);
        TextView tvcuenta = (TextView) findViewById(R.id.textViewcuenta);
        //ver token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
           // usuariotoken.setText(token);
        }else {
           // usuariotoken.setText("Token null");
        }
        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            idusuario.setText("ID: "+id);
            idcliente= id;
        }else {
            idusuario.setText("Id null");
        }
        //ver nombre de usuario
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
            nombreusuario.setText("Usuario: "+username);
        }else {
            nombreusuario.setText("Usuario null");
        }
        //ver el tipo de cuenta
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = preferenciascuenta.getString("CUENTA", "");
        if (cuenta != ""){
            tvcuenta.setText("Tipo de cuenta: "+cuenta);
        }else {
            tvcuenta.setText("Usuario null");
        }

        // mando a llamar el metodo de la lsita de mis comercios
        construirRecyclerComercios();
        Obtenerdatoscliente();
    }//oncrete

    public void llenarcomercios(){
        listacomercios.add(new ComerciosVo("Abarrotes","Tiendas de la esquina, productos que puedes encontrar en toda la tienda de tu esquina",R.drawable.c_abarrotes));
        listacomercios.add(new ComerciosVo("Carnicerias","Tiendas de la esquina, productos que puedes encontrar en toda la tienda de tu esquina",R.drawable.c_carnicerias));
        listacomercios.add(new ComerciosVo("Construccion","Tiendas de la esquina, productos que puedes encontrar en toda la tienda de tu esquina",R.drawable.c_construccion));
        listacomercios.add(new ComerciosVo("Cremerias","Tiendas de la esquina, productos que puedes encontrar en toda la tienda de tu esquina",R.drawable.c_cremerias));
        listacomercios.add(new ComerciosVo("Dulceria","Tiendas de la esquina",R.drawable.c_dulcerias));
        listacomercios.add(new ComerciosVo("Farmacias","Tiendas de la esquina",R.drawable.c_farmarcias));
        listacomercios.add(new ComerciosVo("Ferreterias","Tiendas de la esquina",R.drawable.c_ferreterias));
        listacomercios.add(new ComerciosVo("Panaderia","Tiendas de la esquina",R.drawable.c_panaderias));
        listacomercios.add(new ComerciosVo("Papelerias","Tiendas de la esquina",R.drawable.c_papelerias));
        listacomercios.add(new ComerciosVo("Pinturas","Tiendas de pinturas para el hogar",R.drawable.c_pinturas));
        listacomercios.add(new ComerciosVo("Veterinarias","Tiendas de la esquina",R.drawable.c_veterinarias));
        listacomercios.add(new ComerciosVo("Tortilleria","Tiendas de la esquina",R.drawable.c_panaderias));


    }

    //construyo mis comercios
    public void construirRecyclerComercios(){
        //--    CODIGO DEL RECYCLEVIEW
        listacomercios = new ArrayList<>();
        recyclerViewComercios = (RecyclerView) findViewById(R.id.RecyclerId);
        recyclerViewComercios.setLayoutManager(new LinearLayoutManager(this));
        llenarcomercios();
        AdaptadorComercios adapter = new AdaptadorComercios(listacomercios);
        //metodo del de onclik de mi lista de comercios
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent go = new Intent(Panel.this,Tiendas.class);
                go.putExtra("girocomercio",listacomercios.get(recyclerViewComercios.getChildAdapterPosition(v)).getNombre());
                startActivity(go);

            }
        });
        recyclerViewComercios.setAdapter(adapter);


    }

    //-----------------------------------------------------------------------PETICION PARA LOS DATOS DEL CLIENTE Y SUS METODOS
    public void Obtenerdatoscliente(){
        // petiocion para obtener los datos del cliente y guardarlos

        ServicioPeticion service = Api.getApi(Panel.this).create(ServicioPeticion.class);
        Call<VM_Datoscliente> iniciarCall = service.get_Datoscliente(idcliente);
        iniciarCall.enqueue(new Callback<VM_Datoscliente>() {
            @Override
            public void onResponse(Call<VM_Datoscliente> call, Response<VM_Datoscliente> response) {
                VM_Datoscliente peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Panel.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    ESTADO = peticion.estado;
                    guardarPreferenestado();
                    MUNICIPIO= peticion.municipio;
                    guardarPreferenmunicipio();
                    COLONIA = peticion.colonia;
                    guardarPreferencolonia();
                    CALLE = peticion.calle;
                    guardarPreferencalle();
                    CP = peticion.cp;
                    guardarPreferencp();
                }
            }

            @Override
            public void onFailure(Call<VM_Datoscliente> call, Throwable t) {
                Toast.makeText(Panel.this,"Ups! algo salio mal",Toast.LENGTH_LONG).show();
            }
        });

    }

    // guardar nombre de estado
    public void guardarPreferenestado(){
        SharedPreferences preferenciaestado = getSharedPreferences("credencialesestado", Context.MODE_PRIVATE);
        String estado = ESTADO;
        SharedPreferences.Editor editor = preferenciaestado.edit();
        editor.putString("ESTADO", estado);
        editor.commit();
    }
    public void guardarPreferenmunicipio(){
        SharedPreferences preferenciamunicipio = getSharedPreferences("credencialesmunicipio", Context.MODE_PRIVATE);
        String municipio = MUNICIPIO;
        SharedPreferences.Editor editor = preferenciamunicipio.edit();
        editor.putString("MUNICIPIO", municipio);
        editor.commit();
    }
    public void guardarPreferencolonia(){
        SharedPreferences preferenciacolonia = getSharedPreferences("credencialescolonia", Context.MODE_PRIVATE);
        String colonia = COLONIA;
        SharedPreferences.Editor editor = preferenciacolonia.edit();
        editor.putString("COLONIA", colonia);
        editor.commit();
    }
    public void guardarPreferencalle(){
        SharedPreferences preferenciacalle = getSharedPreferences("credencialescalle", Context.MODE_PRIVATE);
        String calle = CALLE;
        SharedPreferences.Editor editor = preferenciacalle.edit();
        editor.putString("CALLE", calle);
        editor.commit();
    }
    public void guardarPreferencp(){
        SharedPreferences preferenciacp = getSharedPreferences("credencialescp", Context.MODE_PRIVATE);
        String cp = CP;
        SharedPreferences.Editor editor = preferenciacp.edit();
        editor.putString("CP", cp);
        editor.commit();
    }
}//Clase