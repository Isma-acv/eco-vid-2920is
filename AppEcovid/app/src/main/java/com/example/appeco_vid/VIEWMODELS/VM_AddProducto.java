package com.example.appeco_vid.VIEWMODELS;

public class VM_AddProducto {

    public String id;
    public String usuario;
    public String token;
    public String nomproducto;
    public String precio;
    public String contenido;

    public VM_AddProducto(String id, String usuario, String token, String nomproducto, String precio, String contenido) {
        this.id = id;
        this.usuario = usuario;
        this.token = token;
        this.nomproducto = nomproducto;
        this.precio = precio;
        this.contenido = contenido;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNomproducto() {
        return nomproducto;
    }

    public void setNomproducto(String nomproducto) {
        this.nomproducto = nomproducto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String estado;
    public String detalle;

}
