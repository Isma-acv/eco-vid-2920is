package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_ActivarCuenta;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivarCuenta extends AppCompatActivity {
    public String idusuario,tipocuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activar_cuenta);
        //obtengo datos
        TextView holaname = (TextView) findViewById(R.id.textViewHolaName);
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
            holaname.setText("HOLA! "+username);
        }
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
            idusuario = id;
        }
        //ver el tipo de cuenta
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = preferenciascuenta.getString("CUENTA", "");
        if (cuenta != ""){
           tipocuenta = cuenta;
        }

    }//oncreate


    public void ActivarCueta(View view){
        EditText Edtcode = (EditText) findViewById(R.id.editTextTextCodigoActivacion);
        String code = Edtcode.getText().toString();
        //verifico que no sean nulos

        if(TextUtils.isEmpty(code)){
            Edtcode.setError("Porfavor Ingrese su codigo");
            Edtcode.requestFocus();
            return;
        }

        //--incio peticon de logeo
        ServicioPeticion service = Api.getApi(ActivarCuenta.this).create(ServicioPeticion.class);
        Call<VM_ActivarCuenta> iniciarCall = service.get_Activar(idusuario,code);
        iniciarCall.enqueue(new Callback<VM_ActivarCuenta>() {
            @Override
            public void onResponse(Call<VM_ActivarCuenta> call, Response<VM_ActivarCuenta> response) {
                VM_ActivarCuenta peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(ActivarCuenta.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estado == "true"){
                    Toast.makeText(ActivarCuenta.this,"Felicidades la cuenta se activo"+tipocuenta,Toast.LENGTH_LONG).show();

                            if (peticion.cuenta.equals("cliente")){
                                Intent go = new Intent(ActivarCuenta.this,Completar_Cliente.class);
                                startActivity(go);
                            }else{
                                Intent gio = new Intent(ActivarCuenta.this,Completarcomerciante.class);
                                startActivity(gio);
                            }

                }else {
                    Toast.makeText(ActivarCuenta.this,"Error en autenticación de codigo",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VM_ActivarCuenta> call, Throwable t) {
                Toast.makeText(ActivarCuenta.this,"Ups! algo salio mal",Toast.LENGTH_LONG).show();
            }
        });

    }//activar cuenta



}//clase
